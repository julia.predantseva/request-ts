"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createAnObjectFromBlob = function (data) {
    const fileUrl = URL.createObjectURL(data);
    const { type } = data;
    return {
        fileUrl,
        type
    };
};
