import { BaseComponent } from 'predantseva-my-framework';
import { ProgressBar } from '@components';
import '../style.scss';
import { IUploadState } from 'src/interfaces';
interface IUploadProp {
    uploadform: IUploadState;
    formUploadFile: ({ file: {}, onUploadProgress: {} }: {
        file: {};
        onUploadProgress: {};
    }) => void;
}
declare class UploadForm extends BaseComponent<IUploadProp> {
    input: HTMLInputElement;
    progressBar: ProgressBar;
    constructor(props: IUploadProp | {}, node: HTMLElement);
    onsubmit(e: Event): void;
    render(): void;
}
export default UploadForm;
//# sourceMappingURL=index.d.ts.map