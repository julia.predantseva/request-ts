export declare const FORM_UPLOADED_REQUEST: string;
export declare const FORM_UPLOADED_FAILURE: string;
export declare const FORM_UPLOADED_SUCCESS: string;
export declare const formUploadFile: (payload: {
    file: File;
    onUploadProgress: ({}: {}) => void;
}) => {
    type: symbol;
    types: string[];
    payload: () => Promise<{
        name: string;
        data: {};
    }>;
};
//# sourceMappingURL=uploadForm.d.ts.map