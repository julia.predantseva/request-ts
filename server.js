const webpack = require('webpack');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const express = require('express');
const fileUpload = require('express-fileupload');
const bodyParser = require('body-parser');

const app = express();
const path = require('path');
const fs = require('fs');
const config = require('./webpack.config.js');

app.use('/files', express.static(path.join(__dirname, '/uploads')));
app.use('/favicon.ico', express.static(path.join(__dirname, '/assets/favicon.ico')));
app.use('/logo.png', express.static(path.join(__dirname, '/assets/logo.png')));
app.use('/rat.svg', express.static(path.join(__dirname, '/assets/rat.svg')));
app.use('/login', express.static(path.join(__dirname, '/users')));
app.use(bodyParser.json());


if (process.env.NODE_ENV !== 'production') {
  config.entry.app.unshift('webpack-hot-middleware/client?reload=true&timeout=1000');
  config.plugins.push(new webpack.HotModuleReplacementPlugin());
  const compiler = webpack(config);
  app.use(webpackDevMiddleware(compiler, {
    publicPath: config.output.publicPath
  }));
  app.use(webpackHotMiddleware(compiler));
}
else {
  app.use('/form', express.static(path.join(__dirname, '/public/index.html')));
  app.use('/main.css', express.static(`${__dirname}/public/main.css`));
  app.use('/app.bundle.js', express.static(path.join(__dirname, '/public/app.bundle.js')));
}


app.use(fileUpload());

app.post('/upload', function(req, res) {
  let sampleFile = null;
  let uploadPath = null;

  if (Object.keys(req.files).length === 0) {
    res.status(400).send('No files were uploaded.');
    return;
  }

  sampleFile = req.files.sampleFile; // eslint-disable-line
  uploadPath = path.join(__dirname, '/uploads/', sampleFile.name);
  sampleFile.mv(uploadPath, function(err) {
    if (err) {
      return res.status(500).send({ err, status: 500 });
    }
    res.send(sampleFile);
  });
});

const users = [
  { firstName: 'julia', lastName: 'predantseva', email: 'onatskaya.julia@gmail.com', password: '123' },
  { firstName: 'Some', lastName: 'Name', email: 'some@gmail.com', password: 'some' }
];

app.post('/login', function(req, res) {
  const { email, password } = req.body;
  const user = users.find(user => (email === user.email && password === user.password));

  if (!user) {
    return res.status(401).send({ error: 'Incorrect Login or password', status: 401 });
  }

  const { firstName, lastName } = user;
  res.status(200).send({ firstName, lastName });
});

app.get('/list', function(req, res) {
  const listPath = path.join(__dirname, '/uploads');
  fs.readdir(listPath, function(err, files) {
    if (err) {
      throw new Error(err);
    }
    res.send(files.filter(files => files[0] !== '.'));
  });
});

app.listen(8000, function() {
  console.log('Express server listening on port 8000'); // eslint-disable-line
});
