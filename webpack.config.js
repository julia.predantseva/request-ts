const path = require('path');
const publicPath = path.resolve(__dirname, 'public');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const modeEnv = process.env.NODE_ENV === 'production';
// const { CheckerPlugin } = require('awesome-typescript-loader');


module.exports = {
  mode: modeEnv ? 'production' : 'development',
  entry: {
    app: ['./src/index.ts']
    // app: ['./src/index.js']
  },
  devtool: modeEnv ? 'eval' : 'inline-source-map',
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: 'awesome-typescript-loader'
      },
      {
        test: /\.json$/,
        loader: 'json-loader'
      },
      // {
      //   test: /\.js$/,
      //   loader: 'babel-loader'
      // },
      {
        test: /\.html$/,
        exclude: /node_modules/,
        use: { loader: 'html-loader' }
      },
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: ['css-loader', 'sass-loader']
        })
      }
      // {
      //   test: /\.tsx?$/,
      //   loader: 'awesome-typescript-loader'
      // }
    ]
  },
  resolve: {
    extensions: ['.js', '.ts', '.json', '.jsx', '.scss'],
    alias: {
      '@api': path.resolve(__dirname, 'src/api'),
      '@core': path.resolve(__dirname, 'src/core'),
      '@config': path.resolve(__dirname, 'config'),
      '@store': path.resolve(__dirname, 'src/store/'),
      '@utils': path.resolve(__dirname, 'src/utils/'),
      '@actionsUpload': path.resolve(__dirname, 'src/actions/uploadForm'),
      '@actionsUser': path.resolve(__dirname, 'src/actions/user'),
      '@actionsList': path.resolve(__dirname, 'src/actions/list'),
      '@actionsDownload': path.resolve(__dirname, 'src/actions/downloadForm'),
      '@reducers': path.resolve(__dirname, 'src/reducers'),
      '@components': path.resolve(__dirname, 'src/components'),
      '@containers': path.resolve(__dirname, 'src/containers')
    }
  },
  devServer: {
    contentBase: publicPath
  },
  plugins: [
    new CleanWebpackPlugin({ cleanOnceBeforeBuildPatterns: ['public'] }),
    new HtmlWebpackPlugin({
      title: modeEnv ? 'Production' : 'Development',
      template: path.resolve(__dirname, 'index.ejs')
    }),
    new ExtractTextPlugin('style.css')
    // new CheckerPlugin()
  ],
  output: {
    filename: '[name].bundle.js',
    path: publicPath
  }
};

// const path = require('path');
// const HtmlWebpackPlugin = require('html-webpack-plugin');
// const MiniCssExtractPlugin = require('mini-css-extract-plugin');
// const { CleanWebpackPlugin } = require('clean-webpack-plugin');
// const publicPath = path.resolve(__dirname, 'public');

// const mode = process.env.NODE_ENV === 'production';

// module.exports = {
//   entry: {
//     app: ['./src/index.ts']
//   },
//   mode: 'development',
//   devtool: mode ? false : 'inline-source-map',
//   devServer: {
//     contentBase: publicPath
//   },
//   plugins: [
//     new CleanWebpackPlugin({ cleanOnceBeforeBuildPatterns: ['public'] }),
//     new HtmlWebpackPlugin({
//       title: mode ? 'production' : 'develop',
//       template: path.resolve(__dirname, 'index.ejs')
//     }),
//     new MiniCssExtractPlugin({
//       filename: 'main.css'
//     })
//   ],
//   output: {
//     filename: '[name].bundle.js',
//     path: publicPath
//   },
//   module: {
//     rules: [
//       {
//         test: /\.js$/,
//         use: {
//           loader: 'babel-loader'
//         }
//       },
//       {
//         test: /\.(sa|sc|c)ss$/,
//         use: [
//           {
//             loader: MiniCssExtractPlugin.loader,
//             options: {
//               publicPath: '../'
//             }
//           },
//           'css-loader',
//           'sass-loader'
//         ]
//       },
//       {
//         test: /\.(html)$/,
//         use: {
//           loader: 'html-loader',
//           options: {
//             attrs: [':data-src']
//           }
//         }
//       }
//     ]
//   },
//   resolve: {
//     alias: {
//       '@api': path.resolve(__dirname, 'src/api/'),
//       '@utils': path.resolve(__dirname, 'src/utils/'),
//       '@store': path.resolve(__dirname, 'src/store/'),
//       '@config': path.resolve(__dirname, 'config/'),
//       '@actions': path.resolve(__dirname, 'src/actions/'),
//       '@reducers': path.resolve(__dirname, 'src/reducers/'),
//       '@components': path.resolve(__dirname, 'src/components'),
//       '@containers': path.resolve(__dirname, 'src/containers')
//     }
//   }
// };