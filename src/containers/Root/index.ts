import { BaseComponent } from 'predantseva-my-framework';
import { connect } from '@store';
import { logIn, logOut } from '@actionsUser';
import { Img, DownloadForm, UploadForm, List, Authentication, Header } from '@components';
import template from './root.template.html';
import { IUserState } from 'src/interfaces';


interface IPropTypes {
  user: IUserState,
  logIn: ({})=> void;
  logOut: ()=> void;
};

@connect<IUserState, IPropTypes>(({ user }) => ({ user }), { logIn, logOut })
class Root extends BaseComponent<IPropTypes> {

  render() {
    if(!this.props){
      return;
    }
    const { user, logIn, logOut } = this.props;
    const { userInfo, errorMessage } = user;
    this.node.innerHTML = template;

    const headerProps = userInfo ?
      { firstName: userInfo.firstName, lastName: userInfo.lastName, status: true, logOut } :
      { status: false, logOut };

    const header = new Header({ headerProps }, document.querySelector('header') as HTMLElement);

    if (!userInfo) {
      const login = new Authentication({ logIn, errorMessage }, this.node.querySelector('.login') as HTMLElement);
      return;
    }
    const image = new Img({}, this.node.querySelector('.downloaded-img') as HTMLElement);
    const list = new List({}, this.node.querySelector('.files') as HTMLElement);
    const downloadForm = new DownloadForm({}, this.node.querySelector('.downloadForm') as HTMLElement);
    const uploadForm = new UploadForm({}, this.node.querySelector('.uploadedForm') as HTMLElement);
  }
}

export default Root;