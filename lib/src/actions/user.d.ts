import { IUserData } from 'src/interfaces/user';
export declare const USER_AUTHENTICATION_SUCCESS: string;
export declare const USER_AUTHENTICATION_REQUEST: string;
export declare const USER_AUTHENTICATION_FAILURE: string;
export declare const logIn: (payload: IUserData) => {
    type: symbol;
    types: string[];
    payload: () => Promise<{
        firstName: string;
        lastName: string;
    }>;
};
export declare const LOG_OUT: string;
export declare const logOut: () => {
    type: string;
};
//# sourceMappingURL=user.d.ts.map