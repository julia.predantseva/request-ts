export interface IListState {
    data: Array<string>;
    loading: boolean;
    selected: string;
}