import { BaseComponent } from 'predantseva-my-framework';
import { formUploadFile } from '@actionsUpload';
import { ProgressBar } from '@components';
import { connect } from '@store';
import template from './template.html';
import '../style.scss';
import { IUploadState } from 'src/interfaces';

interface IUploadProp{
    uploadform: IUploadState,
    formUploadFile: ({file: {}, onUploadProgress: {}})=> void;
  };

@connect<IUploadState, IUploadProp>(({ uploadform}) => ({ uploadform }), { formUploadFile })
class UploadForm extends BaseComponent<IUploadProp> {
   public input: HTMLInputElement;
   public progressBar!: ProgressBar;
   constructor(props: IUploadProp | {}, node: HTMLElement){
      super(props, node)
      this.input = this.node.querySelector('.file-input') as HTMLInputElement;
    }

  onsubmit(e: Event) {
    e.preventDefault();
    const onUploadProgress = (e: {total: number, loaded: number}) => {
      const { progressBar } = this;
      progressBar.props.total = e.total;
      progressBar.props.loaded = e.loaded;
    };

    if(!this.input.files){
      return;
    }
    const [file] = this.input.files;
    this.props.formUploadFile({ file, onUploadProgress });
  }

  render() {
    this.node.innerHTML = template;
    this.progressBar = new ProgressBar(
      { loaded: 0, total: 0, percents: true }, 
      this.node.querySelector('.progress-wrapper') as HTMLElement
      );
    (<HTMLElement>this.node.querySelector('.form')).onsubmit = (e: Event) => this.onsubmit(e);
  }
}

export default UploadForm;
