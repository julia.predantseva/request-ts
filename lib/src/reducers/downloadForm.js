"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const predantseva_my_framework_1 = require("predantseva-my-framework");
const _actionsDownload_1 = require("@actionsDownload");
const _actionsList_1 = require("@actionsList");
const _actionsUser_1 = require("@actionsUser");
const initialState = {
    input: '',
    downloadedFile: null,
    errorMessage: '',
    loading: false,
    shouldUpdate: true
};
const downloadActions = {
    [_actionsDownload_1.FORM_DOWNLOADED_REQUEST]: () => ({ loading: true, errorMessage: '', shouldUpdate: true, downloadedFile: null }),
    [_actionsDownload_1.FORM_DOWNLOADED_SUCCESS]: (_, { payload, payload: { type } }) => {
        if (type.match('image')) {
            return ({ loading: false, input: '' });
        }
        return ({ loading: false, downloadedFile: payload });
    },
    [_actionsDownload_1.FORM_DOWNLOADED_FAILURE]: () => ({ loading: false,
        errorMessage: 'Didn\'t find any file with such name',
        input: '' }),
    [_actionsDownload_1.FORM_CHANGED_INPUT_MANUALLY]: (_, { payload: input }) => ({ input, shouldUpdate: false }),
    [_actionsList_1.SELECT_LIST_ITEM]: (_, { payload: input }) => ({ input, errorMessage: '', downloadedFile: null }),
    [_actionsUser_1.LOG_OUT]: () => initialState
};
exports.default = predantseva_my_framework_1.createReducer(downloadActions, initialState);
