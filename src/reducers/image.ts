import { createReducer } from 'predantseva-my-framework';
import { FORM_DOWNLOADED_SUCCESS, FORM_DOWNLOADED_REQUEST } from '@actionsDownload';
import { SELECT_LIST_ITEM } from '@actionsList';
import { LOG_OUT } from '@actionsUser';
import { FORM_UPLOADED_REQUEST } from '@actionsUpload';
import { IImageState } from 'src/interfaces';


const initialState: IImageState = {
  src: ' '
};

const imageActions: {} = {
  [FORM_DOWNLOADED_SUCCESS]: (_:IImageState, {payload: { type, fileUrl }}: {payload: {type: string, fileUrl: string}}) => {
    if (!type.match('image')) {
      return initialState;
    }
    return { src: fileUrl };
  },
  [FORM_DOWNLOADED_REQUEST]: () => initialState,
  [FORM_UPLOADED_REQUEST]: () => initialState,
  [SELECT_LIST_ITEM]: () => initialState,
  [LOG_OUT]: () => initialState
};

export default createReducer(imageActions, initialState);