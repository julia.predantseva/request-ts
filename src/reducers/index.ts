import downloadform from './downloadForm';
import uploadform from './uploadedForm';
import image from './image';
import list from './list';
import user from './user';

export default {
  downloadform,
  uploadform,
  image,
  list,
  user
};