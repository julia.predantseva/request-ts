declare class HttpRequest {
    headers: {};
    baseUrl: string;
    constructor({ baseUrl, headers }: {
        baseUrl: string;
        headers: {
            [key: string]: string;
        };
    });
    get<T>(url: string, config?: {}): Promise<T>;
    post<T>(url: string, config?: {}): Promise<T>;
    private takeResult;
}
export default HttpRequest;
//# sourceMappingURL=HttpRequest.d.ts.map