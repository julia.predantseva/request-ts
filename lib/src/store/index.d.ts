import { Store } from 'predantseva-my-framework';
export declare const store: Store;
export declare const connect: <T, C>(fn: (state: {
    [name: string]: T;
}) => any, actions: import("predantseva-my-framework/lib/src/connect").IActions) => (Cl: any) => void;
//# sourceMappingURL=index.d.ts.map