import { BaseComponent } from 'predantseva-my-framework';
interface IProgressProps {
    loaded: number;
    total: number;
    percents: boolean;
}
declare class ProgressBar extends BaseComponent<IProgressProps> {
    render(): void;
}
export default ProgressBar;
//# sourceMappingURL=index.d.ts.map