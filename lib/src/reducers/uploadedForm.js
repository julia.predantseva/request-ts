"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const predantseva_my_framework_1 = require("predantseva-my-framework");
const _actionsUpload_1 = require("@actionsUpload");
const initialState = {
    loading: false
};
const downloadActions = {
    [_actionsUpload_1.FORM_UPLOADED_REQUEST]: () => ({ loading: true }),
    [_actionsUpload_1.FORM_UPLOADED_FAILURE]: () => initialState,
    [_actionsUpload_1.FORM_UPLOADED_SUCCESS]: () => initialState
};
exports.default = predantseva_my_framework_1.createReducer(downloadActions, initialState);
