import { createReducer } from 'predantseva-my-framework';
import { LOAD_LIST_REQUEST, LOAD_LIST_SUCCESS, LOAD_LIST_FAILURE, SELECT_LIST_ITEM } from '@actionsList';
import { FORM_UPLOADED_SUCCESS } from '@actionsUpload';
import { LOG_OUT } from '@actionsUser';
import { IListState } from 'src/interfaces';



const initialState: IListState = {
  data: [],
  loading: false,
  selected: ''
};

const listActions: {} = {
  [LOAD_LIST_REQUEST]: () => ({ loading: true }),
  [LOAD_LIST_FAILURE]: () => ({ loading: false }),
  [LOAD_LIST_SUCCESS]: (_:IListState, { payload: data }: {payload: string[]} ) => ({ data, loading: false }),
  [SELECT_LIST_ITEM]: (_:IListState, {payload: selected}: {payload: string}) => ({ selected }),
  [LOG_OUT]: () => initialState,
  [FORM_UPLOADED_SUCCESS]: ({data}: {data: IListState['data']}, {payload}: {payload: {name: string}}) => {
    const {name} = payload;
    if (data.includes(name)) {
      return;
    }

    const newData = [...data, name].sort();

    return { data: newData };
  }
};


export default createReducer(listActions, initialState);