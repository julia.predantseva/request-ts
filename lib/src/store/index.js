"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const predantseva_my_framework_1 = require("predantseva-my-framework");
const _reducers_1 = __importDefault(require("@reducers"));
const middlewares = [predantseva_my_framework_1.apiMiddleware];
if (process.env.NODE_ENV !== 'production') {
    middlewares.unshift(predantseva_my_framework_1.loggerMiddleware);
}
exports.store = new predantseva_my_framework_1.Store(_reducers_1.default, middlewares);
exports.connect = predantseva_my_framework_1.createConnect(exports.store);
