import { BaseComponent } from 'predantseva-my-framework';
import template from './template.html';

interface IProgressProps{
  loaded: number;
  total: number;
  percents: boolean;
};

class ProgressBar extends BaseComponent<IProgressProps> {
  render() {
    this.node.innerHTML = template;
    const progressIndicator = this.node.querySelector('progress') as HTMLProgressElement;
    const { loaded, total, percents } = this.props;
    const progressValue = Math.round((loaded * 100) / total) || 0;
    progressIndicator.value = progressValue;

    if (!percents) {
      return;
    }
    const progressPer = this.node.querySelector('#uploadProgressPercents') as HTMLElement ;

    if (progressValue > 0) {
      progressPer.innerText = `${progressValue} %`;
    }
  }
}

export default ProgressBar;

