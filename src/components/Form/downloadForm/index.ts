import { BaseComponent } from 'predantseva-my-framework';
import { downloadFile } from '@utils';
import { ProgressBar } from '@components';
import { formDownloadFile, changedInputManually } from '@actionsDownload';
import { connect } from '@store';
import template from './template.html';
import '../style.scss';
import { IDownloadState } from 'src/interfaces';


interface IDownloadProp {
  downloadform: IDownloadState,
  // formDownloadFile: ({ searchFileUrl: {}, onDownloadProgress: {} }) => void;
  formDownloadFile: typeof formDownloadFile;
  changedInputManually: (el: string)=> void;
};

@connect<IDownloadState, IDownloadProp>(({downloadform}) => ({ downloadform}), { formDownloadFile,
  changedInputManually }
)
class DownloadForm extends BaseComponent<IDownloadProp> {
  public progressBar!: ProgressBar;
  constructor(props: IDownloadProp | {}, node: HTMLElement){
    super(props, node);
    console.log(this)
  }

  componentDidUpdate() {
    const { downloadedFile, input } = this.props.downloadform;

    if (!downloadedFile) {
      return;
    }
    downloadFile({ ...downloadedFile, fileName: input });
    (<HTMLInputElement>this.node.querySelector('.searchInput')).value = '';
  }

  componentShouldUpdate() {
    const { shouldUpdate } = this.props.downloadform;

    if (shouldUpdate) {
      return true;
    }
    return false;
  }

  onsubmit(e: Event) {
    const { input } = this.props.downloadform;
    e.preventDefault();
    const onDownloadProgress = (e: {total: number, loaded: number}) => {
      const { progressBar } = this;
      progressBar.props.total = e.total;
      progressBar.props.loaded = e.loaded;
    };

    this.props.formDownloadFile({ searchFileUrl: input, onDownloadProgress });
  }

  inputChange(e: Event) {
    this.props.changedInputManually((<HTMLInputElement>e.target).value);
  }

  render() {
    const { errorMessage, input } = this.props.downloadform;
    this.node.innerHTML = template;
    (<HTMLElement>this.node.querySelector('.error-message')).innerText = errorMessage;
    const textInput = this.node.querySelector('.searchInput') as HTMLInputElement
    textInput.value = input;
    textInput.oninput = (e: Event) => this.props.changedInputManually((<HTMLInputElement>e.target).value);
    this.progressBar = new ProgressBar(
      { loaded: 0, total: 0, percents: false },
      this.node.querySelector('.progress-wrapper') as HTMLElement
    );
    (<HTMLElement>this.node.querySelector('.download-form')).onsubmit = (e: Event) => this.onsubmit(e);
  }
}

export default DownloadForm;