"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _utils_1 = require("@utils");
class HttpRequest {
    constructor({ baseUrl, headers }) {
        this.takeResult = (method, url, config) => {
            const { transformResponse = [], headers, params, data, responseType = 'json', onUploadProgress = null, onDownloadProgress = null } = config;
            const fullUrl = _utils_1.getFullUrl(this.baseUrl, url, params);
            const xhr = new XMLHttpRequest();
            xhr.open(method, fullUrl);
            const allHeaders = Object.assign(Object.assign({}, this.headers), headers);
            for (const key in allHeaders) {
                xhr.setRequestHeader(key, allHeaders[key]);
            }
            const newData = allHeaders['Content-Type'] !== 'application/json' ? data : JSON.stringify(data);
            xhr.responseType = responseType;
            xhr.upload.onprogress = onUploadProgress;
            xhr.onprogress = onDownloadProgress;
            return new Promise((resolve, reject) => {
                xhr.onload = () => {
                    if (xhr.status !== 200) {
                        reject(xhr.response);
                    }
                    const result = transformResponse.reduce((accum, func) => func(accum), xhr.response);
                    resolve(result);
                };
                xhr.onerror = () => {
                    const err = new Error(xhr.response);
                    reject(err);
                };
                xhr.send(newData);
            });
        };
        this.baseUrl = baseUrl;
        this.headers = headers || {};
    }
    get(url, config = {}) {
        return this.takeResult('GET', url, config);
    }
    post(url, config = {}) {
        return this.takeResult('POST', url, config);
    }
}
exports.default = HttpRequest;
