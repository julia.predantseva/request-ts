export declare const createAnObjectFromBlob: (data: {
    type: string;
}) => {
    fileUrl: string;
    type: string;
};
//# sourceMappingURL=createAnObjectFromBlob.d.ts.map