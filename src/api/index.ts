import HttpRequest from './HttpRequest';
import { createAnObjectFromBlob } from '@utils';
import { config } from '@config';
import { IUserData } from 'src/interfaces/user';

class Api {
  private api: HttpRequest;
  constructor() {
    this.api = new HttpRequest({ baseUrl: config.baseUrl, headers:{} });
  }
  getList(){
    return this.api.get<string[]>('/list');
  }
  getFile({ searchFileUrl, onDownloadProgress } : { searchFileUrl: string, onDownloadProgress: (e: { total: number; loaded: number; })=> void }) {
    const config = {
      onDownloadProgress,
      responseType: "blob" as XMLHttpRequestResponseType,
      transformResponse: [createAnObjectFromBlob]
    };

    return this.api.get<{fileUrl: string, type: string}>(`/files/${searchFileUrl}`, config);
  }
  sendFile({file, onUploadProgress}: { file: File, onUploadProgress?: ({})=> void }) {
    const form = new FormData();
    form.append('sampleFile', file);
    return this.api.post<{name: string, data: {}}>('/upload', { data: form, onUploadProgress });
  }
  login(data: IUserData) {
    const config = {
      data,
      headers: { 'Content-Type': 'application/json' }
    };

    return this.api.post<{firstName: string, lastName: string}>('/login', config);
  }
}

const xhr = new Api();
export default xhr;
