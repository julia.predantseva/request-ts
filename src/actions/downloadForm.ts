import { API_CALL } from 'predantseva-my-framework';
import api from '@api';

export const FORM_DOWNLOADED_REQUEST: string = 'form.downloaded.request';
export const FORM_DOWNLOADED_FAILURE: string = 'form.downloaded.failure';
export const FORM_DOWNLOADED_SUCCESS: string = 'form.downloaded.success';


export const formDownloadFile = (payload: {searchFileUrl: string, onDownloadProgress: (e: { total: number; loaded: number; })=> void}) => ({
  type: API_CALL,
  types: [FORM_DOWNLOADED_REQUEST, FORM_DOWNLOADED_SUCCESS, FORM_DOWNLOADED_FAILURE],
  payload: () => api.getFile(payload)
});

export const FORM_CHANGED_INPUT_MANUALLY: string = 'form.download.changed.input.manually';
export const changedInputManually = (payload: string) => ({ type: FORM_CHANGED_INPUT_MANUALLY, payload });