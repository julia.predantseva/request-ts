"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const HttpRequest_1 = __importDefault(require("./HttpRequest"));
const _utils_1 = require("@utils");
const _config_1 = require("@config");
class Api {
    constructor() {
        this.api = new HttpRequest_1.default({ baseUrl: _config_1.config.baseUrl, headers: {} });
    }
    getList() {
        return this.api.get('/list');
    }
    getFile({ searchFileUrl, onDownloadProgress }) {
        const config = {
            onDownloadProgress,
            responseType: "blob",
            transformResponse: [_utils_1.createAnObjectFromBlob]
        };
        return this.api.get(`/files/${searchFileUrl}`, config);
    }
    sendFile({ file, onUploadProgress }) {
        const form = new FormData();
        form.append('sampleFile', file);
        return this.api.post('/upload', { data: form, onUploadProgress });
    }
    login(data) {
        const config = {
            data,
            headers: { 'Content-Type': 'application/json' }
        };
        return this.api.post('/login', config);
    }
}
const xhr = new Api();
exports.default = xhr;
