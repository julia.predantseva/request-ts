"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const predantseva_my_framework_1 = require("predantseva-my-framework");
const _utils_1 = require("@utils");
const _components_1 = require("@components");
const _actionsDownload_1 = require("@actionsDownload");
const _store_1 = require("@store");
const template_html_1 = __importDefault(require("./template.html"));
require("../style.scss");
;
let DownloadForm = class DownloadForm extends predantseva_my_framework_1.BaseComponent {
    constructor(props, node) {
        super(props, node);
        console.log(this);
    }
    componentDidUpdate() {
        const { downloadedFile, input } = this.props.downloadform;
        if (!downloadedFile) {
            return;
        }
        _utils_1.downloadFile(Object.assign(Object.assign({}, downloadedFile), { fileName: input }));
        this.node.querySelector('.searchInput').value = '';
    }
    componentShouldUpdate() {
        const { shouldUpdate } = this.props.downloadform;
        if (shouldUpdate) {
            return true;
        }
        return false;
    }
    onsubmit(e) {
        const { input } = this.props.downloadform;
        e.preventDefault();
        const onDownloadProgress = (e) => {
            const { progressBar } = this;
            progressBar.props.total = e.total;
            progressBar.props.loaded = e.loaded;
        };
        this.props.formDownloadFile({ searchFileUrl: input, onDownloadProgress });
    }
    inputChange(e) {
        this.props.changedInputManually(e.target.value);
    }
    render() {
        const { errorMessage, input } = this.props.downloadform;
        this.node.innerHTML = template_html_1.default;
        this.node.querySelector('.error-message').innerText = errorMessage;
        const textInput = this.node.querySelector('.searchInput');
        textInput.value = input;
        textInput.oninput = (e) => this.props.changedInputManually(e.target.value);
        this.progressBar = new _components_1.ProgressBar({ loaded: 0, total: 0, percents: false }, this.node.querySelector('.progress-wrapper'));
        this.node.querySelector('.download-form').onsubmit = (e) => this.onsubmit(e);
    }
};
DownloadForm = __decorate([
    _store_1.connect(({ downloadform }) => ({ downloadform }), { formDownloadFile: _actionsDownload_1.formDownloadFile,
        changedInputManually: _actionsDownload_1.changedInputManually }),
    __metadata("design:paramtypes", [Object, HTMLElement])
], DownloadForm);
exports.default = DownloadForm;
