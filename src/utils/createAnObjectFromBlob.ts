export const createAnObjectFromBlob = function(data: { type: string}) {
  const fileUrl = URL.createObjectURL(data);
  const { type } = data;

  return {
    fileUrl,
    type
  };
};