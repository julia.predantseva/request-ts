"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const predantseva_my_framework_1 = require("predantseva-my-framework");
const template_html_1 = __importDefault(require("./template.html"));
;
class Header extends predantseva_my_framework_1.BaseComponent {
    render() {
        const { status, firstName, lastName, logOut } = this.props.headerProps;
        this.node.innerHTML = template_html_1.default;
        const authorizationStatus = this.node.querySelector('.header__login-text');
        if (!status) {
            authorizationStatus.innerText = 'LogIn';
            return;
        }
        authorizationStatus.innerText = 'LogOut';
        this.node.querySelector('.header__login-text-name').innerText = `${firstName} ${lastName}`;
        authorizationStatus.onclick = logOut;
    }
}
exports.default = Header;
