"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const predantseva_my_framework_1 = require("predantseva-my-framework");
const _api_1 = __importDefault(require("@api"));
exports.LOAD_LIST_SUCCESS = 'load.list.success';
exports.LOAD_LIST_REQUEST = 'load.list.request';
exports.LOAD_LIST_FAILURE = 'load.list.failure';
exports.loadListOfFiles = () => ({
    type: predantseva_my_framework_1.API_CALL,
    types: [exports.LOAD_LIST_REQUEST, exports.LOAD_LIST_SUCCESS, exports.LOAD_LIST_FAILURE],
    payload: () => _api_1.default.getList()
});
exports.SELECT_LIST_ITEM = 'select.list.item';
exports.listItemSelected = (payload) => ({ type: exports.SELECT_LIST_ITEM, payload });
