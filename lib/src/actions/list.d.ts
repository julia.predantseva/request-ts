export declare const LOAD_LIST_SUCCESS: string;
export declare const LOAD_LIST_REQUEST: string;
export declare const LOAD_LIST_FAILURE: string;
export declare const loadListOfFiles: () => {
    type: symbol;
    types: string[];
    payload: () => Promise<string[]>;
};
export declare const SELECT_LIST_ITEM: string;
export declare const listItemSelected: (payload: string) => {
    type: string;
    payload: string;
};
//# sourceMappingURL=list.d.ts.map