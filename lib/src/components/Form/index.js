"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const downloadForm_1 = __importDefault(require("./downloadForm"));
exports.DownloadForm = downloadForm_1.default;
const uploadForm_1 = __importDefault(require("./uploadForm"));
exports.UploadForm = uploadForm_1.default;
