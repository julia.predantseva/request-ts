"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const predantseva_my_framework_1 = require("predantseva-my-framework");
const _actionsDownload_1 = require("@actionsDownload");
const _actionsList_1 = require("@actionsList");
const _actionsUser_1 = require("@actionsUser");
const _actionsUpload_1 = require("@actionsUpload");
const initialState = {
    src: ' '
};
const imageActions = {
    [_actionsDownload_1.FORM_DOWNLOADED_SUCCESS]: (_, { payload: { type, fileUrl } }) => {
        if (!type.match('image')) {
            return initialState;
        }
        return { src: fileUrl };
    },
    [_actionsDownload_1.FORM_DOWNLOADED_REQUEST]: () => initialState,
    [_actionsUpload_1.FORM_UPLOADED_REQUEST]: () => initialState,
    [_actionsList_1.SELECT_LIST_ITEM]: () => initialState,
    [_actionsUser_1.LOG_OUT]: () => initialState
};
exports.default = predantseva_my_framework_1.createReducer(imageActions, initialState);
