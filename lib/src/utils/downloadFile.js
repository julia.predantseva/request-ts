"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.downloadFile = (sendedData) => {
    const { fileName, fileUrl } = sendedData;
    const link = document.createElement('a');
    link.href = fileUrl;
    link.download = fileName || 'download';
    link.click();
    link.remove();
};
