import { BaseComponent } from 'predantseva-my-framework';
import template from './template.html';

interface IHeaderProp {
  headerProps: {
    firstName?: string,
    lastName?: string,
    status: boolean,
    logOut: ()=>void;
  }
};



class Header extends BaseComponent<IHeaderProp> {
  render() {
    const { status, firstName, lastName, logOut } = this.props.headerProps;
    this.node.innerHTML = template;
    const authorizationStatus = this.node.querySelector('.header__login-text') as HTMLElement;

    if (!status) {
      authorizationStatus.innerText = 'LogIn';
      return;
    }
    authorizationStatus.innerText = 'LogOut';
    (<HTMLElement>this.node.querySelector('.header__login-text-name')).innerText = `${firstName} ${lastName}`;

    authorizationStatus.onclick = logOut;
  }
}


export default Header;
