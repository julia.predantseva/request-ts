export const getFullUrl = (baseUrl: string, additionUrl: string, params?:{[key: string]: string }) => {
  const fullUrl = baseUrl + additionUrl;
  const url = new URL(fullUrl);

  for (const key in params) {
    url.searchParams.set(key, params[key]);
  }
  return fullUrl;
};
