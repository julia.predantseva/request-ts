"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const predantseva_my_framework_1 = require("predantseva-my-framework");
const _api_1 = __importDefault(require("@api"));
exports.FORM_UPLOADED_REQUEST = 'form.uploaded.request';
exports.FORM_UPLOADED_FAILURE = 'form.uploaded.failure';
exports.FORM_UPLOADED_SUCCESS = 'form.uploaded.success';
exports.formUploadFile = (payload) => ({
    type: predantseva_my_framework_1.API_CALL,
    types: [exports.FORM_UPLOADED_REQUEST, exports.FORM_UPLOADED_SUCCESS, exports.FORM_UPLOADED_FAILURE],
    payload: () => _api_1.default.sendFile(payload)
});
