import { createReducer } from 'predantseva-my-framework';
import {
  USER_AUTHENTICATION_REQUEST,
  USER_AUTHENTICATION_SUCCESS,
  USER_AUTHENTICATION_FAILURE,
  LOG_OUT
} from '@actionsUser';
import { IUserState } from 'src/interfaces';

const initialState:IUserState  = {
  loading: false,
  userInfo: null,
  errorMessage: ''
};

const userActions: {} = {
  [USER_AUTHENTICATION_REQUEST]: () => ({ loading: true, errorMessage: '', userInfo: null }),
  [USER_AUTHENTICATION_SUCCESS]: (_:IUserState, {payload: userInfo}: {payload: { firstName: string, lastName: string }}) => ({ loading: false, userInfo }),
  [USER_AUTHENTICATION_FAILURE]: (_:IUserState, {payload: {error}} : {payload:{error: string}}) => ({ loading: false, errorMessage: error }),
  [LOG_OUT]: () => initialState
};

export default createReducer(userActions, initialState);