import { createReducer } from 'predantseva-my-framework';
import { FORM_UPLOADED_REQUEST, FORM_UPLOADED_FAILURE, FORM_UPLOADED_SUCCESS } from '@actionsUpload';
import { IUploadState } from 'src/interfaces';

const initialState:IUploadState = {
  loading: false
};

const downloadActions = {
  [FORM_UPLOADED_REQUEST]: () => ({ loading: true }),
  [FORM_UPLOADED_FAILURE]: () => initialState,
  [FORM_UPLOADED_SUCCESS]: () => initialState
};

export default createReducer(downloadActions, initialState);