import { BaseComponent } from 'predantseva-my-framework';
interface IAuthenticationProp {
    errorMessage: string;
    logIn: (params: {
        password: string;
        email: string;
    }) => void;
}
declare class Authentication extends BaseComponent<IAuthenticationProp> {
    render(): void;
}
export default Authentication;
//# sourceMappingURL=index.d.ts.map