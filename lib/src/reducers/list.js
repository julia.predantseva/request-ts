"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const predantseva_my_framework_1 = require("predantseva-my-framework");
const _actionsList_1 = require("@actionsList");
const _actionsUpload_1 = require("@actionsUpload");
const _actionsUser_1 = require("@actionsUser");
const initialState = {
    data: [],
    loading: false,
    selected: ''
};
const listActions = {
    [_actionsList_1.LOAD_LIST_REQUEST]: () => ({ loading: true }),
    [_actionsList_1.LOAD_LIST_FAILURE]: () => ({ loading: false }),
    [_actionsList_1.LOAD_LIST_SUCCESS]: (_, { payload: data }) => ({ data, loading: false }),
    [_actionsList_1.SELECT_LIST_ITEM]: (_, { payload: selected }) => ({ selected }),
    [_actionsUser_1.LOG_OUT]: () => initialState,
    [_actionsUpload_1.FORM_UPLOADED_SUCCESS]: ({ data }, { payload }) => {
        const { name } = payload;
        if (data.includes(name)) {
            return;
        }
        const newData = [...data, name].sort();
        return { data: newData };
    }
};
exports.default = predantseva_my_framework_1.createReducer(listActions, initialState);
