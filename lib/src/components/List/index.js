"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const predantseva_my_framework_1 = require("predantseva-my-framework");
const _actionsList_1 = require("@actionsList");
const _store_1 = require("@store");
require("./style.scss");
let List = class List extends predantseva_my_framework_1.BaseComponent {
    componentDidMount() {
        this.props.loadListOfFiles();
    }
    render() {
        const { data } = this.props.list;
        this.node.innerHTML = '';
        const list = document.createElement('ol');
        list.className = 'files__list';
        const fragment = document.createDocumentFragment();
        data.forEach((content) => {
            const listItem = document.createElement('li');
            listItem.className = 'files__list-item';
            listItem.innerText = content;
            listItem.onclick = () => this.props.listItemSelected(content);
            fragment.append(listItem);
        });
        list.append(fragment);
        this.node.append(list);
    }
};
List = __decorate([
    _store_1.connect(({ list }) => ({ list }), { loadListOfFiles: _actionsList_1.loadListOfFiles, listItemSelected: _actionsList_1.listItemSelected })
], List);
exports.default = List;
