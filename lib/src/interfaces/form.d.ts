export interface IDownloadState {
    input: string;
    downloadedFile: {
        fileName: string;
        fileUrl: string;
    } | null;
    errorMessage: string;
    loading: boolean;
    shouldUpdate: boolean;
}
export interface IUploadState {
    loading: boolean;
}
//# sourceMappingURL=form.d.ts.map