export declare const getFullUrl: (baseUrl: string, additionUrl: string, params?: {
    [key: string]: string;
} | undefined) => string;
//# sourceMappingURL=getFullUrl.d.ts.map