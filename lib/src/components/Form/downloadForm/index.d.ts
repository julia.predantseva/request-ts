import { BaseComponent } from 'predantseva-my-framework';
import { ProgressBar } from '@components';
import { formDownloadFile } from '@actionsDownload';
import '../style.scss';
import { IDownloadState } from 'src/interfaces';
interface IDownloadProp {
    downloadform: IDownloadState;
    formDownloadFile: typeof formDownloadFile;
    changedInputManually: (el: string) => void;
}
declare class DownloadForm extends BaseComponent<IDownloadProp> {
    progressBar: ProgressBar;
    constructor(props: IDownloadProp | {}, node: HTMLElement);
    componentDidUpdate(): void;
    componentShouldUpdate(): boolean;
    onsubmit(e: Event): void;
    inputChange(e: Event): void;
    render(): void;
}
export default DownloadForm;
//# sourceMappingURL=index.d.ts.map