import { BaseComponent } from 'predantseva-my-framework';
import './style.scss';
import { IImageState } from 'src/interfaces';
export interface IImageProp {
    image: IImageState;
}
declare class Img extends BaseComponent<IImageProp> {
    render(): void;
}
export default Img;
//# sourceMappingURL=index.d.ts.map