import { BaseComponent } from 'predantseva-my-framework';
import { loadListOfFiles, listItemSelected } from '@actionsList';
import { connect } from '@store';
import { IListState } from 'src/interfaces';

import './style.scss';

interface IListProps {
  list: IListState,
  listItemSelected: (item: string)=>void;
  loadListOfFiles: ()=> [];
}

@connect<IListState, IListProps>(( {list} ) => ({ list }), { loadListOfFiles, listItemSelected })
class List extends BaseComponent<IListProps> {
  componentDidMount() {
    this.props.loadListOfFiles();
  }

  render() {
    const { data } = this.props.list;
    this.node.innerHTML = '';
    const list = document.createElement('ol');
    list.className = 'files__list';
    const fragment = document.createDocumentFragment();

    data.forEach((content: string) => {
      const listItem = document.createElement('li');
      listItem.className = 'files__list-item';
      listItem.innerText = content;
      listItem.onclick = () => this.props.listItemSelected(content);
      fragment.append(listItem);
    });
    list.append(fragment);
    this.node.append(list);
  }
}

export default List;

