export interface IUserState {
    loading: boolean;
    userInfo: {
        firstName: string;
        lastName: string;
    } | null;
    errorMessage: string;
}
export interface IUserData {
    email: string;
    password: string;
}
//# sourceMappingURL=user.d.ts.map