import { BaseComponent } from 'predantseva-my-framework';
import template from './template.html';

interface IAuthenticationProp{
  errorMessage: string,
  logIn: ( params: {password: string, email: string})=> void;
};

class Authentication extends BaseComponent<IAuthenticationProp> {

  render() {
    const { errorMessage } = this.props;
    this.node.innerHTML = template;
    const email = (<HTMLInputElement>this.node.querySelector('.email'));
    const password = (<HTMLInputElement>this.node.querySelector('.password'));
    (<HTMLElement>this.node.querySelector('.error-message')).innerText = errorMessage;

    (<HTMLElement>this.node.querySelector('.login__form')).onsubmit = (e: Event) => {
      e.preventDefault();
      this.props.logIn({ password: password.value, email: email.value });
    };
  }
}

export default Authentication;
