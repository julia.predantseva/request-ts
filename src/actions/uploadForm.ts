import { API_CALL } from 'predantseva-my-framework';
import api from '@api';

export const FORM_UPLOADED_REQUEST:string = 'form.uploaded.request';
export const FORM_UPLOADED_FAILURE:string = 'form.uploaded.failure';
export const FORM_UPLOADED_SUCCESS:string = 'form.uploaded.success';


export const formUploadFile = (payload: {file: File, onUploadProgress: ({})=> void} ) => ({
  type: API_CALL,
  types: [FORM_UPLOADED_REQUEST, FORM_UPLOADED_SUCCESS, FORM_UPLOADED_FAILURE],
  payload: () => api.sendFile(payload)
});