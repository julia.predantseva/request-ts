"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const predantseva_my_framework_1 = require("predantseva-my-framework");
const _api_1 = __importDefault(require("@api"));
exports.FORM_DOWNLOADED_REQUEST = 'form.downloaded.request';
exports.FORM_DOWNLOADED_FAILURE = 'form.downloaded.failure';
exports.FORM_DOWNLOADED_SUCCESS = 'form.downloaded.success';
exports.formDownloadFile = (payload) => ({
    type: predantseva_my_framework_1.API_CALL,
    types: [exports.FORM_DOWNLOADED_REQUEST, exports.FORM_DOWNLOADED_SUCCESS, exports.FORM_DOWNLOADED_FAILURE],
    payload: () => _api_1.default.getFile(payload)
});
exports.FORM_CHANGED_INPUT_MANUALLY = 'form.download.changed.input.manually';
exports.changedInputManually = (payload) => ({ type: exports.FORM_CHANGED_INPUT_MANUALLY, payload });
