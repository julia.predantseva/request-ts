"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const predantseva_my_framework_1 = require("predantseva-my-framework");
const template_html_1 = __importDefault(require("./template.html"));
;
class ProgressBar extends predantseva_my_framework_1.BaseComponent {
    render() {
        this.node.innerHTML = template_html_1.default;
        const progressIndicator = this.node.querySelector('progress');
        const { loaded, total, percents } = this.props;
        const progressValue = Math.round((loaded * 100) / total) || 0;
        progressIndicator.value = progressValue;
        if (!percents) {
            return;
        }
        const progressPer = this.node.querySelector('#uploadProgressPercents');
        if (progressValue > 0) {
            progressPer.innerText = `${progressValue} %`;
        }
    }
}
exports.default = ProgressBar;
