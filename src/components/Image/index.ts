import { BaseComponent } from 'predantseva-my-framework';
import { connect } from '@store';
import './style.scss';
import { IImageState } from 'src/interfaces';

export interface IImageProp {
  image: IImageState
}

@connect<IImageState, IImageProp>(({image}) => ({ image }), {})
class Img extends BaseComponent<IImageProp> {
  render() {
    const { image } = this.props;
    this.node.innerHTML = '';
    const img = new Image();
    img.classList.add('image');
    img.src = image.src || '';
    this.node.append(img);
  }
}

export default Img;
