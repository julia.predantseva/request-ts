import { BaseComponent } from 'predantseva-my-framework';
import { IListState } from 'src/interfaces';
import './style.scss';
interface IListProps {
    list: IListState;
    listItemSelected: (item: string) => void;
    loadListOfFiles: () => [];
}
declare class List extends BaseComponent<IListProps> {
    componentDidMount(): void;
    render(): void;
}
export default List;
//# sourceMappingURL=index.d.ts.map