import { Store, createConnect, loggerMiddleware, apiMiddleware } from 'predantseva-my-framework';

import reducers from '@reducers';
const middlewares = [apiMiddleware];

if (process.env.NODE_ENV !== 'production') {
  middlewares.unshift(loggerMiddleware);
}

export const store = new Store(reducers, middlewares);

export const connect = createConnect(store);