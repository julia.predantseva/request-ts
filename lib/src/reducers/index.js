"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const downloadForm_1 = __importDefault(require("./downloadForm"));
const uploadedForm_1 = __importDefault(require("./uploadedForm"));
const image_1 = __importDefault(require("./image"));
const list_1 = __importDefault(require("./list"));
const user_1 = __importDefault(require("./user"));
exports.default = {
    downloadform: downloadForm_1.default,
    uploadform: uploadedForm_1.default,
    image: image_1.default,
    list: list_1.default,
    user: user_1.default
};
