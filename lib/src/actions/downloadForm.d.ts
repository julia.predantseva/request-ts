export declare const FORM_DOWNLOADED_REQUEST: string;
export declare const FORM_DOWNLOADED_FAILURE: string;
export declare const FORM_DOWNLOADED_SUCCESS: string;
export declare const formDownloadFile: (payload: {
    searchFileUrl: string;
    onDownloadProgress: (e: {
        total: number;
        loaded: number;
    }) => void;
}) => {
    type: symbol;
    types: string[];
    payload: () => Promise<{
        fileUrl: string;
        type: string;
    }>;
};
export declare const FORM_CHANGED_INPUT_MANUALLY: string;
export declare const changedInputManually: (payload: string) => {
    type: string;
    payload: string;
};
//# sourceMappingURL=downloadForm.d.ts.map