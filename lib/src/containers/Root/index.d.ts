import { BaseComponent } from 'predantseva-my-framework';
import { IUserState } from 'src/interfaces';
interface IPropTypes {
    user: IUserState;
    logIn: ({}: {}) => void;
    logOut: () => void;
}
declare class Root extends BaseComponent<IPropTypes> {
    render(): void;
}
export default Root;
//# sourceMappingURL=index.d.ts.map