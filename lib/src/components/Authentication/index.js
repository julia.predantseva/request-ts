"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const predantseva_my_framework_1 = require("predantseva-my-framework");
const template_html_1 = __importDefault(require("./template.html"));
;
class Authentication extends predantseva_my_framework_1.BaseComponent {
    render() {
        const { errorMessage } = this.props;
        this.node.innerHTML = template_html_1.default;
        const email = this.node.querySelector('.email');
        const password = this.node.querySelector('.password');
        this.node.querySelector('.error-message').innerText = errorMessage;
        this.node.querySelector('.login__form').onsubmit = (e) => {
            e.preventDefault();
            this.props.logIn({ password: password.value, email: email.value });
        };
    }
}
exports.default = Authentication;
