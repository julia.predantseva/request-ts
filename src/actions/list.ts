import { API_CALL } from 'predantseva-my-framework';
import api from '@api';

export const LOAD_LIST_SUCCESS: string = 'load.list.success';
export const LOAD_LIST_REQUEST: string = 'load.list.request';
export const LOAD_LIST_FAILURE: string = 'load.list.failure';

export const loadListOfFiles = () => ({
  type: API_CALL,
  types: [LOAD_LIST_REQUEST, LOAD_LIST_SUCCESS, LOAD_LIST_FAILURE],
  payload: () => api.getList()
});

export const SELECT_LIST_ITEM: string = 'select.list.item';
export const listItemSelected = (payload: string) => ({ type: SELECT_LIST_ITEM, payload });

