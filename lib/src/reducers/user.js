"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const predantseva_my_framework_1 = require("predantseva-my-framework");
const _actionsUser_1 = require("@actionsUser");
const initialState = {
    loading: false,
    userInfo: null,
    errorMessage: ''
};
const userActions = {
    [_actionsUser_1.USER_AUTHENTICATION_REQUEST]: () => ({ loading: true, errorMessage: '', userInfo: null }),
    [_actionsUser_1.USER_AUTHENTICATION_SUCCESS]: (_, { payload: userInfo }) => ({ loading: false, userInfo }),
    [_actionsUser_1.USER_AUTHENTICATION_FAILURE]: (_, { payload: { error } }) => ({ loading: false, errorMessage: error }),
    [_actionsUser_1.LOG_OUT]: () => initialState
};
exports.default = predantseva_my_framework_1.createReducer(userActions, initialState);
