import { createReducer } from 'predantseva-my-framework';
import {
  FORM_DOWNLOADED_REQUEST,
  FORM_DOWNLOADED_FAILURE,
  FORM_DOWNLOADED_SUCCESS,
  FORM_CHANGED_INPUT_MANUALLY
} from '@actionsDownload';
import { SELECT_LIST_ITEM } from '@actionsList';
import { LOG_OUT } from '@actionsUser';
import { IDownloadState } from 'src/interfaces';


const initialState: IDownloadState = {
  input: '',
  downloadedFile: null,
  errorMessage: '',
  loading: false,
  shouldUpdate: true
};

const downloadActions: {} = {
  [FORM_DOWNLOADED_REQUEST]: () => ({ loading: true, errorMessage: '', shouldUpdate: true, downloadedFile: null }),
  [FORM_DOWNLOADED_SUCCESS]: (_: IDownloadState, { payload,payload: { type }}: {payload: { type: string }}) => {
    if (type.match('image')) {
      return ({ loading: false, input: '' });
    }
    return ({ loading: false, downloadedFile: payload });
  },
  [FORM_DOWNLOADED_FAILURE]: () => ({ loading: false,
    errorMessage: 'Didn\'t find any file with such name',
    input: '' }),
  [FORM_CHANGED_INPUT_MANUALLY]: (_:IDownloadState, {payload: input}: {payload: string} ) => ({ input, shouldUpdate: false }),
  [SELECT_LIST_ITEM]: (_:IDownloadState, {payload: input}: {payload: string}) => ({ input, errorMessage: '', downloadedFile: null }),
  [LOG_OUT]: () => initialState
};

export default createReducer(downloadActions, initialState);