import DownloadForm from './downloadForm';
import UploadForm from './uploadForm';

export { DownloadForm, UploadForm };