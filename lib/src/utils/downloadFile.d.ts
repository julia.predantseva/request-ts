export declare const downloadFile: (sendedData: {
    fileName: string;
    fileUrl: string;
}) => void;
//# sourceMappingURL=downloadFile.d.ts.map