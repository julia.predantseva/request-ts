"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const predantseva_my_framework_1 = require("predantseva-my-framework");
const _store_1 = require("@store");
const _actionsUser_1 = require("@actionsUser");
const _components_1 = require("@components");
const root_template_html_1 = __importDefault(require("./root.template.html"));
;
let Root = class Root extends predantseva_my_framework_1.BaseComponent {
    render() {
        if (!this.props) {
            return;
        }
        const { user, logIn, logOut } = this.props;
        const { userInfo, errorMessage } = user;
        this.node.innerHTML = root_template_html_1.default;
        const headerProps = userInfo ?
            { firstName: userInfo.firstName, lastName: userInfo.lastName, status: true, logOut } :
            { status: false, logOut };
        const header = new _components_1.Header({ headerProps }, document.querySelector('header'));
        if (!userInfo) {
            const login = new _components_1.Authentication({ logIn, errorMessage }, this.node.querySelector('.login'));
            return;
        }
        const image = new _components_1.Img({}, this.node.querySelector('.downloaded-img'));
        const list = new _components_1.List({}, this.node.querySelector('.files'));
        const downloadForm = new _components_1.DownloadForm({}, this.node.querySelector('.downloadForm'));
        const uploadForm = new _components_1.UploadForm({}, this.node.querySelector('.uploadedForm'));
    }
};
Root = __decorate([
    _store_1.connect(({ user }) => ({ user }), { logIn: _actionsUser_1.logIn, logOut: _actionsUser_1.logOut })
], Root);
exports.default = Root;
