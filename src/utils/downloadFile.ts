export const downloadFile = (sendedData: {fileName: string, fileUrl: string}) => {
  const {
    fileName,
    fileUrl
  } = sendedData;

  const link = document.createElement('a');
  link.href = fileUrl;
  link.download = fileName || 'download';
  link.click();
  link.remove();
};
