"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getFullUrl = (baseUrl, additionUrl, params) => {
    const fullUrl = baseUrl + additionUrl;
    const url = new URL(fullUrl);
    for (const key in params) {
        url.searchParams.set(key, params[key]);
    }
    return fullUrl;
};
