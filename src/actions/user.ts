import { API_CALL } from 'predantseva-my-framework';
import api from '@api';
import { IUserData } from 'src/interfaces/user';

export const USER_AUTHENTICATION_SUCCESS:string = 'user.authentication.success';
export const USER_AUTHENTICATION_REQUEST:string = 'user.authentication.request';
export const USER_AUTHENTICATION_FAILURE:string = 'user.authentication.failure';


export const logIn = (payload: IUserData) => ({
  type: API_CALL,
  types: [USER_AUTHENTICATION_REQUEST, USER_AUTHENTICATION_SUCCESS, USER_AUTHENTICATION_FAILURE],
  payload: () => api.login(payload)
});

export const LOG_OUT: string = 'log.out';
export const logOut = () => ({ type: LOG_OUT });