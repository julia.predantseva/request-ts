"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const predantseva_my_framework_1 = require("predantseva-my-framework");
const _store_1 = require("@store");
require("./style.scss");
let Img = class Img extends predantseva_my_framework_1.BaseComponent {
    render() {
        const { image } = this.props;
        this.node.innerHTML = '';
        const img = new Image();
        img.classList.add('image');
        img.src = image.src || '';
        this.node.append(img);
    }
};
Img = __decorate([
    _store_1.connect(({ image }) => ({ image }), {})
], Img);
exports.default = Img;
