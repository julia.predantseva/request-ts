"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const predantseva_my_framework_1 = require("predantseva-my-framework");
const _api_1 = __importDefault(require("@api"));
exports.USER_AUTHENTICATION_SUCCESS = 'user.authentication.success';
exports.USER_AUTHENTICATION_REQUEST = 'user.authentication.request';
exports.USER_AUTHENTICATION_FAILURE = 'user.authentication.failure';
exports.logIn = (payload) => ({
    type: predantseva_my_framework_1.API_CALL,
    types: [exports.USER_AUTHENTICATION_REQUEST, exports.USER_AUTHENTICATION_SUCCESS, exports.USER_AUTHENTICATION_FAILURE],
    payload: () => _api_1.default.login(payload)
});
exports.LOG_OUT = 'log.out';
exports.logOut = () => ({ type: exports.LOG_OUT });
