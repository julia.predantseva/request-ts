"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const predantseva_my_framework_1 = require("predantseva-my-framework");
const _actionsUpload_1 = require("@actionsUpload");
const _components_1 = require("@components");
const _store_1 = require("@store");
const template_html_1 = __importDefault(require("./template.html"));
require("../style.scss");
;
let UploadForm = class UploadForm extends predantseva_my_framework_1.BaseComponent {
    constructor(props, node) {
        super(props, node);
        this.input = this.node.querySelector('.file-input');
    }
    onsubmit(e) {
        e.preventDefault();
        const onUploadProgress = (e) => {
            const { progressBar } = this;
            progressBar.props.total = e.total;
            progressBar.props.loaded = e.loaded;
        };
        if (!this.input.files) {
            return;
        }
        const [file] = this.input.files;
        this.props.formUploadFile({ file, onUploadProgress });
    }
    render() {
        this.node.innerHTML = template_html_1.default;
        this.progressBar = new _components_1.ProgressBar({ loaded: 0, total: 0, percents: true }, this.node.querySelector('.progress-wrapper'));
        this.node.querySelector('.form').onsubmit = (e) => this.onsubmit(e);
    }
};
UploadForm = __decorate([
    _store_1.connect(({ uploadform }) => ({ uploadform }), { formUploadFile: _actionsUpload_1.formUploadFile }),
    __metadata("design:paramtypes", [Object, HTMLElement])
], UploadForm);
exports.default = UploadForm;
