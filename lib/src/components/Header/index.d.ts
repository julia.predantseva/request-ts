import { BaseComponent } from 'predantseva-my-framework';
interface IHeaderProp {
    headerProps: {
        firstName?: string;
        lastName?: string;
        status: boolean;
        logOut: () => void;
    };
}
declare class Header extends BaseComponent<IHeaderProp> {
    render(): void;
}
export default Header;
//# sourceMappingURL=index.d.ts.map