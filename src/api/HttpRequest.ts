import { getFullUrl } from '@utils';

interface IConfig {
  transformResponse?: Function[];
  headers?: {[key: string]: string};
  params?: {[key: string]: string};
  data?: string | FormData | Blob | Document | ArrayBufferView | ArrayBuffer | URLSearchParams | ReadableStream<Uint8Array> | null | undefined;
  responseType?: XMLHttpRequestResponseType; 
  onUploadProgress?: ({})=> void;
  onDownloadProgress?: ({})=> void; 
}

class HttpRequest{
  headers: {};
  baseUrl: string;
  constructor( { baseUrl, headers } : { baseUrl: string, headers: {[key: string]: string}}) {
    this.baseUrl = baseUrl;
    this.headers = headers || {};
  }

  get<T>(url: string, config = {}) {
    return this.takeResult<T>('GET', url, config);
  }

  post<T>(url: string, config = {}) {
    return this.takeResult<T>('POST', url, config);
  }

  private takeResult =<T>(method: string, url: string, config: IConfig) => {
    const {
      transformResponse = [],
      headers,
      params,
      data,
      responseType = 'json',
      onUploadProgress = null,
      onDownloadProgress = null
    } = config;

    const fullUrl = getFullUrl(this.baseUrl, url, params);
    const xhr = new XMLHttpRequest();
    xhr.open(method, fullUrl);

    const allHeaders:{[key: string]: string} = { ...this.headers, ...headers };

    for (const key in allHeaders) {
      xhr.setRequestHeader(key, allHeaders[key]);
    }

    const newData = allHeaders['Content-Type'] !== 'application/json' ? data : JSON.stringify(data);

    xhr.responseType = responseType;
    xhr.upload.onprogress = onUploadProgress;
    xhr.onprogress = onDownloadProgress;

    return new Promise<T>((resolve, reject) => {
      xhr.onload = () => {
        if (xhr.status !== 200) {
          reject(xhr.response);
        }
        const result = <T>transformResponse.reduce((accum, func)=> func(accum), xhr.response);
        resolve(result);
      };
      xhr.onerror = () => {
        const err = new Error(xhr.response)
        reject(err);
      };
      xhr.send(newData);
    });
  }
}

export default HttpRequest;

