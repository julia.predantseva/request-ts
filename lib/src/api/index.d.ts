import { IUserData } from 'src/interfaces/user';
declare class Api {
    private api;
    constructor();
    getList(): Promise<string[]>;
    getFile({ searchFileUrl, onDownloadProgress }: {
        searchFileUrl: string;
        onDownloadProgress: (e: {
            total: number;
            loaded: number;
        }) => void;
    }): Promise<{
        fileUrl: string;
        type: string;
    }>;
    sendFile({ file, onUploadProgress }: {
        file: File;
        onUploadProgress?: ({}: {}) => void;
    }): Promise<{
        name: string;
        data: {};
    }>;
    login(data: IUserData): Promise<{
        firstName: string;
        lastName: string;
    }>;
}
declare const xhr: Api;
export default xhr;
//# sourceMappingURL=index.d.ts.map